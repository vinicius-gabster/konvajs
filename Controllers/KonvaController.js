class KonvaController{

	constructor(){
		this.bibliotecas = [];	 
	}

	async Getbibliotecas(){
		return await $.ajax({
			type: "GET",
			url: URL_API + "/ws/b/c/?format=json",
			contentType: 'json',
			headers: {
				"Authorization": "ApiKey "+USER+":"+APIKEY+"",
				"Content-Type" : 'application/json',
			},
			success: async function (data){
				await console.log(data.status.bibliotecas);
			},
			error: function(data){
				alert("error")
			}
		})
		.done(function(data){
			return data;	
		})

	}

	async getOpcoesComponente(idProduto){
		let response = [];
		return await $.ajax({
			type: "GET",
			url: URL_API + "/api/v1/via_opcao_componente/?componente="+idProduto+"&format=json",
			contentType: 'json',
			headers: {
				"Authorization": "ApiKey "+USER+":"+APIKEY+"",
				"Content-Type" : 'application/json',
			},
			success: async function (data){
				response = data;
				await console.log(data)

			}
		})
		.done(async function(data){
			console.log('done')
			return data;	
		})

		
	}

	async MenuLeft(){
		let bibliotecas = await this.Getbibliotecas();
		console.log('bibliotecas');
		//console.log(bibliotecas.status.bibliotecas[7].grupos[1].subgrupos);
		let subgrupos = bibliotecas.status.bibliotecas;
		console.log(bibliotecas)
		for (var i = 0; i < subgrupos.length; i++) {
			for (var j = 0; j < subgrupos[i].itens.length; j++) {
			console.log(subgrupos[i].itens[j])
			$('.menu-left').append(`
				<div class="produto">
					<img src="${subgrupos[i].itens[j].url_preview}" />
        			<button class="waves-effect waves-light btn btn-adicionar" onclick="AddComponente(${1},${1},'${subgrupos[i].itens[j].url_preview}')"><i class="fas fa-plus-circle"></i></button>
				</div>

				`)
			}
		}

	}
}

let controller = new KonvaController().MenuLeft();