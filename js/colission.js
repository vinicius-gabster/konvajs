function updateFrame(update,destroy,visible=true) {

	/*console.log(update);*/
	try { 
		layer.children[destroy.index].destroy();
	}
	catch(err) {
		console.log(err);
	}
	//layer.draw();


	var frameWidth = update.width;
	var frameHeight = update.height;

	var ratio = 1;

	var frameOnScreenWidth = frameWidth * ratio;
	var frameOnScreenHeight = frameHeight * ratio;
	/*console.log('frameOnScreenWidth: '+frameOnScreenWidth)
	console.log('frameOnScreenHeight: '+frameOnScreenHeight)*/

	group[contador] = new Konva.Group({
		draggable: true,
		group:true,
		width:frameWidth, 
		height:frameHeight,
		dragBoundFunc: function(pos) {
        MAX_WIDTH = document.getElementById('container').clientWidth;
        MAX_HEIGHT = document.getElementById('container').clientHeight;

        let x_abs = pos.x
        let y_abs = pos.y;

        if (pos.x < 0 ) {
          x_abs = 0;
        }

        if (pos.y < 0 ) {
          y_abs = 0;
        }

        /*console.log(pos)
        console.log(this.attrs)
        console.log(MAX_HEIGHT)*/

        if ( pos.y + (this.attrs.height * 0.5) >= MAX_HEIGHT ) {
          y_abs = MAX_HEIGHT - (this.attrs.height *0.5);
/*          console.log(y_abs);
*/        }

        if ((this.attrs.width * 0.5)+ pos.x >= MAX_WIDTH ) {
          x_abs = MAX_WIDTH - (this.attrs.width * 0.5);
/*          console.log(x_abs);
*/
        }

        return {
          x: x_abs,
          y: y_abs
        }
      }
	});
	/*group[contador].x(Math.round(stage.width() / 2 - frameOnScreenWidth / 2) + 0.5);
	group[contador].y(Math.round(stage.height() / 2 - frameOnScreenHeight / 2) + 0.5);*/
	group[contador].x(update.x);
	group[contador].y(update.y);


	var frameGroup = createFrame(frameWidth, frameHeight);
	frameGroup.scale({ x: ratio, y: ratio });

	group[contador].add(frameGroup);

	var infoGroup = createInfo(frameOnScreenWidth, frameOnScreenHeight,visible);
	group[contador].add(infoGroup);

	test = group[contador];
	
	
	group[contador].on('mouseover', function(e) {
		tr.detach(var_attach_to);
		var_attach_to = this;
		tr.attachTo(this);
	});

	group[contador].on('click', function(e) {
		tr.attachTo(this);
		layer.draw();
	});


	group[contador].on('dblclick', function(e) {
		console.log('double click');
	});


	tr.attachTo(group[contador]);
	tr.forceUpdate();
	layer.add(group[contador]);
	layer.draw();
	contador++;

	return group[contador];
}

function createFrame(frameWidth, frameHeight) {

	//console.log(frameWidth);

	var padding = 0;
	var group = new Konva.Group();
	var glass = new Konva.Rect({
		x: padding,
		y: padding,
		width: frameWidth,
		height: frameHeight,
		fill: 'lightblue',
		visible:true,
		name: 'produtoGBS',
		stroke:'red',
		strokeWidth:5,
	});

	group.add(glass);
	return group;
}
/*Altura e largura total do componente*/
function createInfo(frameWidth, frameHeight,_visible=true) {
	/*console.log(frameWidth);    console.log(frameHeight);*/
	var offset = -20;

	var arrowOffset = offset / 2;
	var arrowSize = 10;
	var stroke = 0.0;
	var lineWidth = 0.0;

	var group = new Konva.Group({visible: _visible});
	var lines = new Konva.Shape({
		sceneFunc: function (ctx) {
			ctx.fillStyle = 'black';
			ctx.lineWidth = lineWidth;

			ctx.moveTo(0, 0);
			ctx.lineTo(-offset, 0);

			ctx.moveTo(0, frameHeight);
			ctx.lineTo(-offset, frameHeight);

			ctx.moveTo(0, frameHeight);
			ctx.lineTo(0, frameHeight + offset);

			ctx.moveTo(frameWidth, frameHeight);
			ctx.lineTo(frameWidth, frameHeight + offset);

			ctx.stroke();
		},
		visible: _visible
	});

	var leftArrow = new Konva.Shape({
		sceneFunc: function (ctx) {
					// top pointer
					ctx.moveTo(-arrowOffset - arrowSize, arrowSize);
					ctx.lineTo(-arrowOffset, 0);
					ctx.lineTo(-arrowOffset + arrowSize, arrowSize);

					// line
					ctx.moveTo(-arrowOffset, 0);
					ctx.lineTo(-arrowOffset, frameHeight);

					// bottom pointer
					ctx.moveTo(-arrowOffset - arrowSize, frameHeight - arrowSize);
					ctx.lineTo(-arrowOffset, frameHeight);
					ctx.lineTo(-arrowOffset + arrowSize, frameHeight - arrowSize);

					ctx.strokeShape(this);
				},
				stroke: 'grey',
				strokeWidth: lineWidth,
				visible: _visible
			});

	var bottomArrow = new Konva.Shape({
		sceneFunc: function (ctx) {
					// top pointer
					ctx.translate(0, frameHeight + arrowOffset);
					ctx.moveTo(arrowSize, -arrowSize);
					ctx.lineTo(0, 0);
					ctx.lineTo(arrowSize, arrowSize);

					// line
					ctx.moveTo(0, 0);
					ctx.lineTo(frameWidth, 0);

					// bottom pointer
					ctx.moveTo(frameWidth - arrowSize, -arrowSize);
					ctx.lineTo(frameWidth, 0);
					ctx.lineTo(frameWidth - arrowSize, arrowSize);

					ctx.strokeShape(this);
				},
				stroke: 'grey',
				strokeWidth: lineWidth,
				visible: _visible
			});

	var leftLabel = new Konva.Label();

	var leftText = new Konva.Text({
		fontSize:18,
		text: Math.round(frameHeight) + 'mm',
		padding: 2,
		fill: 'black',
		visible: _visible,
		name: 'leftText'
	});

	leftLabel.add(leftText);
	leftLabel.position({
		x: leftText.x() + 10,
		y: frameHeight / 2 - leftText.height() / 2
	});




	var bottomLabel = new Konva.Label();
	var bottomText = new Konva.Text({
		fontSize:18,
		text: Math.round(frameWidth) + 'mm',
		padding: 2,
		fill: 'black',
		name: 'bottomText'

	});

	bottomLabel.add(bottomText);
	bottomLabel.position({
		x: frameWidth / 2 - bottomText.width() / 2,
		y: frameHeight - 25
	});

	group.add(leftArrow, bottomArrow, leftLabel, bottomLabel);

	return group;
}