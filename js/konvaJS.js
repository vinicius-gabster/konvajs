var width = window.innerWidth;
var height = window.innerHeight;
var test = 0;
var isVisible = true;

var var_attach_to;

var tr = new Konva.Transformer({
	boundBoxFunc:function (oldBoundBox, newBoundBox) {
		MAX_WIDTH = document.getElementById('container').clientWidth

		let id = undefined;

		layer.children.each(function(child){
			if (child.attrs.isGBS != undefined) {


				if (oldBoundBox.x == child.attrs.x) {
					id = child.index;
				}
			}

		});

		if (newBoundBox.width + newBoundBox.x > MAX_WIDTH) {
			newBoundBox = oldBoundBox;
		}

		if (newBoundBox.x < 0 ) {
			newBoundBox = oldBoundBox;
		}

		if (newBoundBox.height + newBoundBox.y > MAX_WIDTH) {
			newBoundBox = oldBoundBox;
		}

		if (newBoundBox.y < 0 ) {
			newBoundBox = oldBoundBox;
		}

		if (newBoundBox.width > MAX_WIDTH) {
			newBoundBox.width = MAX_WIDTH;

		}

		if (newBoundBox.width < -MAX_WIDTH) {
			newBoundBox.width = -MAX_WIDTH;
		}

		layer.children.each(function(g){  
			if(g.attrs.isGBS == true){
				if (g.index != id && id != undefined) {
					let intersection = haveIntersection(g.getClientRect(), newBoundBox)
					if (intersection) {
						console.log(g);
						console.log(oldBoundBox);
						newBoundBox = oldBoundBox;
					}
				}
			}
		})

		return newBoundBox;
	}
});

var stage = new Konva.Stage({
	container: 'container',
	width: width,
	height: height
});


var layer = new Konva.Layer();
var group = new Konva.Group({draggable: true});
stage.add(layer);

var widthInput = document.getElementById('widthInput');
var heightInput = document.getElementById('heightInput');