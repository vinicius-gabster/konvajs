
function haveIntersection(r1, r2) {
  return !(
    r2.x > r1.x + r1.width ||
    r2.x + r2.width < r1.x ||
    r2.y > r1.y + r1.height ||
    r2.y + r2.height < r1.y
    );
}

function  calcDimesao(pos , obj){
  let atributos = obj.attrs;
  let attr_X = (atributos.height * atributos.scaleX) + atributos.x > document.getElementById('container').clientWidth;
  return attr_X;
}

function addComponente(nome_comp){
  eval(`

    let group = [];

    group['${nome_comp}'] = new Konva.Group({
      x: 0,
      y: 0,
      width: 600,
      height: 300,
      draggable: true,
      isGBS: true,
      keepRatio:true,
      dragBoundFunc: function(pos) {

        MAX_WIDTH = document.getElementById('container').clientWidth;
        MAX_HEIGHT = document.getElementById('container').clientHeight;

        let x_abs = pos.x
        let y_abs = pos.y;

        if (pos.x < 0 ) {
          x_abs = 0;
        }

        if (pos.y < 0 ) {
          y_abs = 0;
        }

        if ((this.attrs.height * this.attrs.scaleY)+ pos.y > MAX_HEIGHT ) {
          y_abs = MAX_HEIGHT - (this.attrs.height * this.attrs.scaleY);
        }

        if ((this.attrs.width * this.attrs.scaleX)+ pos.x > MAX_WIDTH ) {
          x_abs = MAX_WIDTH - (this.attrs.width * this.attrs.scaleX);
        }

        return {
          x: x_abs,
          y: y_abs
        }
      }
    });


    componenteCriado = createFrame('${nome_comp}',group['${nome_comp}'].attrs);

    group['${nome_comp}'].add(componenteCriado);

    /*componenteCriado = createFrame2('${nome_comp}',group['${nome_comp}'].attrs);
    group['${nome_comp}'].add(componenteCriado);*/

    var frameHeightRect =  componenteCriado.getClientRect().height;
    var frameWidthRect =  componenteCriado.getClientRect().width;
    var frameY =  componenteCriado.getClientRect().y;
    var frameX =  componenteCriado.getClientRect().x;

    var regua = createInfo(frameWidthRect,frameHeightRect,frameX,frameY);

    group['${nome_comp}'].add(regua);

    group['${nome_comp}'].on('click', function() {
      tr.destroy();

      tr = new Konva.Transformer({
        node: this,
        rotateEnabled:false,
        boundBoxFunc:function (oldBoundBox, newBoundBox) {
          teste2 = tr._node;
          let compResize = tr._node.getChildren();
          

          MAX_WIDTH = document.getElementById('container').clientWidth

          let id = undefined;

          layer.children.each(function(child){
            if (child.attrs.isGBS != undefined) {


              if (oldBoundBox.x == child.attrs.x) {
                id = child.index;
              }
            }
            
          });

          if (newBoundBox.width + newBoundBox.x > MAX_WIDTH) {
            newBoundBox = oldBoundBox;
          }

          if (newBoundBox.x < 0 ) {
            newBoundBox = oldBoundBox;
          }

          if (newBoundBox.height + newBoundBox.y > MAX_WIDTH) {
            newBoundBox = oldBoundBox;
          }

          if (newBoundBox.y < 0 ) {
            newBoundBox = oldBoundBox;
          }

          if (newBoundBox.width > MAX_WIDTH) {
            newBoundBox.width = MAX_WIDTH;

          }

          if (newBoundBox.width < -MAX_WIDTH) {
            newBoundBox.width = -MAX_WIDTH;
          }

          layer.children.each(function(g){  
            if(g.attrs.isGBS == true){
              if (g.index != id && id != undefined) {
                let intersection = haveIntersection(g.getClientRect(), newBoundBox)
                if (intersection) {
                  newBoundBox = oldBoundBox;
                }
              }
            }
          })

          for (var i = 0; i <= compResize.length ; i++) {
            if (compResize[i] != undefined) {

              tr._node.setAttrs(newBoundBox);

              if (compResize[i].attrs.type == 'regua') {
                compResize[i].destroy();
                layer.draw();
                let newRegua = updateInfo(compResize[i-1].attrs.width,compResize[i-1].attrs.height,compResize[i-1].attrs.x,compResize[i-1].attrs.y, tr._node.attrs.width,tr._node.attrs.height);
                console.log('compResize[i-1]')
                console.log(tr._node.attrs.width)
                tr._node.add(newRegua);
                layer.draw();
                
              }
            }

          }

          console.log(newBoundBox);
          
          return newBoundBox;
        },

      });

      tr.attachTo(this);
      layer.add(tr);
      tr.forceUpdate();
      layer.add( group['${nome_comp}']);
      stage.add(layer);   
      layer.draw();
    });

    var_attach_to = componenteCriado;
    layer.add( group['${nome_comp}']);
    stage.add(layer);   
    layer.draw();

    `)
}

function createFrame(nome_comp, group){

  console.log(nome_comp)
  console.log(group)


  var comp = new Konva.Rect({
    x: group.x ,
    y: group.y,
    width: group.width,
    height: group.height,
    fill: '#00000040',
    name: 'rect',
    isGBS:true,
    type:'componente'

  });
  ;

  return comp;
}

function createFrame2(nome_comp, group){

  console.log(nome_comp)
  console.log(group)


  var comp = new Konva.Rect({
    x: group.x + 10,
    y: group.y + 10 ,
    width: group.width-20,
    height: group.height-20,
    fill: 'red',
    name: 'rect',
    isGBS:true,
    type:'componente'

  });
  ;

  return comp;
}

function createInfo(frameWidth, frameHeight, frameX,frameY, _visible=true) {
  var offset = -20;
  var arrowOffset = offset / 2;
  var arrowSize = 5;
  var group = new Konva.Group({visible: _visible,isGBS:true,type:'regua'});


  var leftArrow = new Konva.Shape({
    sceneFunc: function (ctx) {
      /*top pointer*/
      ctx.moveTo(-arrowOffset - arrowSize, arrowSize);
      ctx.lineTo(-arrowOffset, 0);
      ctx.lineTo(-arrowOffset + arrowSize , arrowSize);

      /*line*/
      ctx.moveTo(-arrowOffset, 0);
      ctx.lineTo(-arrowOffset, frameHeight);

      /*bottom pointer*/
      ctx.moveTo(-arrowOffset - arrowSize, frameHeight - arrowSize);
      ctx.lineTo(-arrowOffset, frameHeight);
      ctx.lineTo(-arrowOffset + arrowSize, frameHeight - arrowSize);

      ctx.strokeShape(this);
    },
    stroke: 'grey',
    strokeWidth: 0.5,
    visible: _visible,
    x:frameX,
    y:frameY
  });

  var bottomArrow = new Konva.Shape({
    sceneFunc: function (ctx) {
      /*top pointer*/
      ctx.translate(0, frameHeight + arrowOffset);
      ctx.moveTo(arrowSize, -arrowSize);
      ctx.lineTo(0, 0);
      ctx.lineTo(arrowSize, arrowSize);

      /*line*/
      ctx.moveTo(0, 0);
      ctx.lineTo(frameWidth, 0); /* X*/

      /*bottom pointer*/
      ctx.moveTo(frameWidth - arrowSize, -arrowSize);
      ctx.lineTo(frameWidth, 0);
      ctx.lineTo(frameWidth - arrowSize, arrowSize);

      ctx.strokeShape(this);
    },
    stroke: 'black',
    strokeWidth: 0.5,
    visible: _visible,
    x:frameX,
    y:frameY

  });

  /*left text*/
  var leftLabel = new Konva.Label();

  var leftText = new Konva.Text({
    text: frameHeight.toFixed(2) + 'mm',
    padding: 2,
    fill: 'black',
    visible: _visible,
    fontSize: 12

  });

  leftLabel.add(leftText);
  leftLabel.position({
    x: frameX - (offset/2),
    y: ((frameHeight + frameY )/ 2)
  });




  /*bottom text*/
  var bottomLabel = new Konva.Label();
  var bottomText = new Konva.Text({
    keepRatio:false,
    text: frameWidth.toFixed(2) + 'mm',
    padding: 2,
    fill: 'black'
  });

  bottomLabel.add(bottomText);
  bottomLabel.position({
    x: ((frameX + frameWidth) + (offset * 2 ) )/2,
    y: frameHeight + frameY + offset + -5
  });

  group.add(leftArrow, bottomArrow, leftLabel, bottomLabel);
  console.log('createInfo')
  console.log(group)

  return group;
}

function updateInfo(frameWidth, frameHeight, frameX,frameY, labelX,labelY, _visible=true) {
  var offset = -20;
  var arrowOffset = offset / 2;
  var arrowSize = 5;
  var group = new Konva.Group({visible: _visible,isGBS:true,type:'regua'});


  var leftArrow = new Konva.Shape({
    sceneFunc: function (ctx) {
      /*top pointer*/
      ctx.moveTo(-arrowOffset - arrowSize, arrowSize);
      ctx.lineTo(-arrowOffset, 0);
      ctx.lineTo(-arrowOffset + arrowSize , arrowSize);

      /*line*/
      ctx.moveTo(-arrowOffset, 0);
      ctx.lineTo(-arrowOffset, frameHeight);

      /*bottom pointer*/
      ctx.moveTo(-arrowOffset - arrowSize, frameHeight - arrowSize);
      ctx.lineTo(-arrowOffset, frameHeight);
      ctx.lineTo(-arrowOffset + arrowSize, frameHeight - arrowSize);

      ctx.strokeShape(this);
    },
    stroke: 'grey',
    strokeWidth: 0.5,
    visible: _visible,
    x:frameX,
    y:frameY
  });

  var bottomArrow = new Konva.Shape({
    sceneFunc: function (ctx) {
      /*top pointer*/
      ctx.translate(0, frameHeight + arrowOffset);
      ctx.moveTo(arrowSize, -arrowSize);
      ctx.lineTo(0, 0);
      ctx.lineTo(arrowSize, arrowSize);

      /*line*/
      ctx.moveTo(0, 0);
      ctx.lineTo(frameWidth, 0); /* X*/

      /*bottom pointer*/
      ctx.moveTo(frameWidth - arrowSize, -arrowSize);
      ctx.lineTo(frameWidth, 0);
      ctx.lineTo(frameWidth - arrowSize, arrowSize);

      ctx.strokeShape(this);
    },
    stroke: 'black',
    strokeWidth: 0.5,
    visible: _visible,
    x:frameX,
    y:frameY

  });

  /*left text*/
  var leftLabel = new Konva.Label();

  var leftText = new Konva.Text({
    text: labelY + 'mm',
    padding: 2,
    fill: 'black',
    visible: _visible
  });

  leftLabel.add(leftText);
  leftLabel.position({
    x: frameX - (offset/2),
    y: ((frameHeight + frameY )/ 2)
  });




  /*bottom text*/
  var bottomLabel = new Konva.Label();
  var bottomText = new Konva.Text({
    text: labelX + 'mm',
    padding: 2,
    fill: 'black'
  });

  bottomLabel.add(bottomText);
  bottomLabel.position({
    x: ((frameX + frameWidth) + (offset * 2 ) )/2,
    y: frameHeight + frameY + offset + -5
  });

  group.add(leftArrow, bottomArrow, leftLabel, bottomLabel);
  console.log('createInfo')
  console.log(group)

  return group;
}

function initProjeto(lenX,lenY){

  let view = $('#container');
  let lenX_view = view.width();
  let lenY_view = view.height();

  let razaoY = lenY_view/lenY;
  let razaoX = lenX_view/lenX;

  console.log(razaoX);
  console.log(razaoY);

  let razao = undefined;

  if (razaoX < razaoY) {
    razao = razaoX;
  }else{
    razao = razaoY;
  }

  lenX = lenX*razao;
  lenY = lenY*razao;


  let marginX = Math.abs((lenX - lenX_view))/2;
  let marginY = Math.abs((lenY - lenY_view))/2;

  $('body').append(`

    <nav class="nav">

    <div class="card text-center">
    <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
    <li class="nav-item">
    <a class="nav-link" href="#">Active</a>
    </li>
    <li class="nav-item">
    <a class="nav-link active" href="#">Link</a>
    </li>
    <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
    </li>
    </ul>
    </div>
    <div class="card-body">
    <div class="menu"></div>
    </div>
    </div>

    </nav>

    <div id="container"><div class="konvajs-content" role="presentation" style="position: relative;user-select: none;width: 1350px;height: 625px;"><canvas width="1366" height="625" style="padding: 0px; margin: 0px; border: 0px; background: transparent; position: absolute; top: 0px; left: 0px; width: 1366px; height: 625px; display: block;"></canvas></div></div>


    <style>
    body {
      margin: 0;
      padding: 0;
      overflow: hidden;
      background-color: #EEE;

    }
    #container{

      margin-top: 50px;
      width:${lenX}px;
      height:${lenY}px;
      background-color: #BBB;
      margin-left: 50px;
      margin-right: 50px;
    }
    </style>



    `);
}
