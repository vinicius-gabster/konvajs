 // var lx = prompt("Comprimento da Parede em cm?");
 // var ly = prompt("Altura da Parede em cm?");

 var width = window.innerWidth;
 var height = window.innerHeight;
 var layer = new Konva.Layer();
 var MAX_WIDTH = document.getElementById('container').clientWidth;
 var var_attach_to = undefined;
 var widthInput = 250;
 var heightInput = 500;
 var frameWidth = parseInt(widthInput, 10);
 var frameHeight = parseInt(heightInput, 10);

 var Groups = new Konva.Group();
 let componente = [];

 var stage = new Konva.Stage({
 	container: 'container',
 	width: width,
 	height: height
 });


 var tr = new Konva.Transformer({
 	boundBoxFunc:function (oldBoundBox, newBoundBox) {
 		MAX_WIDTH = document.getElementById('container').clientWidth

 		let id = undefined;

 		layer.children.each(function(child){
 			if (child.attrs.isGBS != undefined) {
                /*console.log(oldBoundBox);
                console.log(child);*/

                if (oldBoundBox.x == child.attrs.x) {
                	id = child.index;
                }
            }
            
        });

 		if (newBoundBox.width + newBoundBox.x > MAX_WIDTH) {
 			newBoundBox = oldBoundBox;
 		}

 		if (newBoundBox.x < 0 ) {
 			newBoundBox = oldBoundBox;
 		}

 		if (newBoundBox.height + newBoundBox.y > MAX_WIDTH) {
 			newBoundBox = oldBoundBox;
 		}

 		if (newBoundBox.y < 0 ) {
 			newBoundBox = oldBoundBox;
 		}

 		if (newBoundBox.width > MAX_WIDTH) {
 			newBoundBox.width = MAX_WIDTH;

 		}

 		if (newBoundBox.width < -MAX_WIDTH) {
 			newBoundBox.width = -MAX_WIDTH;
 		}

 		layer.children.each(function(g){  
 			if(g.attrs.isGBS == true){
 				if (g.index != id && id != undefined) {
 					let intersection = haveIntersection(g.getClientRect(), newBoundBox)
 					if (intersection) {
 						console.log(g);
 						console.log(oldBoundBox);
 						newBoundBox = oldBoundBox;
 					}
 				}
 			}
 		})

 		return newBoundBox;
 	}
 });

 layer.add(tr);
 stage.add(layer);
 layer.draw();

 var pos_valid = undefined;

 layer.on('dragmove', function(e) {
    
 	tr.detach(var_attach_to);
 	var target = e.target;
 	var targetRect = e.target.getClientRect();

 	let arr = layer.children;
 	let arrayRects = [];
 	let _lastPosValid = undefined;

 	arr.each((x)=>{

 		if (x.getAttr('isGBS')) {
 			arrayRects.push(x);  
 		}
 	});

 	for (var i = arrayRects.length - 1; i >= 0; i--) {
 		let arrX1 = arrayRects[i].getClientRect().x  
 		let arrY1 = arrayRects[i].getClientRect().y  
 		let arrWidth = arrayRects[i].getClientRect().width
 		let arrHeight = arrayRects[i].getClientRect().height

 		let objX1 = targetRect.x  
 		let objY1 = targetRect.y  
 		let objWidth = targetRect.width
 		let objHeight = targetRect.height

 		if (arrX1 == objX1 && arrY1 == objY1 && arrWidth == objWidth && arrHeight == objHeight) {

 		}else{
 			let intersection = haveIntersection(arrayRects[i].getClientRect(),targetRect);
 			if (intersection) {
 				target.setAbsolutePosition({x:pos_valid.x,y:pos_valid.y});
 			} else {
 				pos_valid = target._lastPos;
 			}
 		}
 	}
 });



 function haveIntersection(r1, r2) {
 	return !(
 		r2.x > r1.x + r1.width ||
 		r2.x + r2.width < r1.x ||
 		r2.y > r1.y + r1.height ||
 		r2.y + r2.height < r1.y
 		);
 }

 function  calcDimesao(pos , obj){
 	let atributos = obj.attrs;
 	let attr_X = (atributos.height * atributos.scaleX) + atributos.x > document.getElementById('container').clientWidth;
 	return attr_X;
 }

 function initProjeto(lenX,lenY){

 	let view = $('#container');
 	let lenX_view = view.width();
 	let lenY_view = view.height();

 	let razaoY = lenY_view/lenY;
 	let razaoX = lenX_view/lenX;

 	console.log(razaoX);
 	console.log(razaoY);

 	let razao = undefined;

 	if (razaoX < razaoY) {
 		razao = razaoX;
 	}else{
 		razao = razaoY;
 	}

 	lenX = lenX*razao;
 	lenY = lenY*razao;


 	let marginX = Math.abs((lenX - lenX_view))/2;
 	let marginY = Math.abs((lenY - lenY_view))/2;

 	$('body').append(`

 		<nav class="nav">

 		<div class="card text-center">
 		<div class="card-header">
 		<ul class="nav nav-tabs card-header-tabs">
 		<li class="nav-item">
 		<a class="nav-link" href="#">Active</a>
 		</li>
 		<li class="nav-item">
 		<a class="nav-link active" href="#">Link</a>
 		</li>
 		<li class="nav-item">
 		<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
 		</li>
 		</ul>
 		</div>
 		<div class="card-body">
 		<div class="menu"></div>
 		</div>
 		</div>

 		</nav>

 		<div id="container">
 		</div>


 		<style>
 		body {
 			margin: 0;
 			padding: 0;
 			overflow: hidden;
 			background-color: #EEE;

 		}
 		#container{

 			margin-top: 50px;
 			width:${lenX}px;
 			height:${lenY}px;
 			background-color: #BBB;
 			margin-left: 50px;
 			margin-right: 50px;
 		}
 		</style>



 		`);

 }

 function addComponente(nome_comp){
 	eval(`


 		var createComponente = new Konva.Rect({
 			x: 160,
 			y: 60,
 			width: 100,
 			height: 90,
 			fill: '#00000040',
 			name: 'rect',
 			draggable: true,
 			isGBS: true,
 			dragBoundFunc: function(pos) {
 				let x_abs = pos.x
 				let y_abs = pos.y;

 				if (pos.x < 0 ) {
 					x_abs = 0;
 				}

 				if (pos.y < 0 ) {
 					y_abs = 0;
 				}

 				if ((this.attrs.height * this.attrs.scaleY)+ pos.y > document.getElementById('container').clientHeight ) {
 					y_abs = document.getElementById('container').clientHeight - (this.attrs.height * this.attrs.scaleY);
 				}

 				if ((this.attrs.width * this.attrs.scaleX)+ pos.x > document.getElementById('container').clientWidth ) {
 					x_abs = document.getElementById('container').clientWidth - (this.attrs.width * this.attrs.scaleX);
 				}

 				return {
 					x: x_abs,
 					y: y_abs
 				}


 			}
 		});

 		//group.add(line,test);

 		createComponente.on('click', function() {
 			tr.detach(var_attach_to);
 			var_attach_to = this;
 			tr.attachTo(this);
 		});



 		//layer.add(createComponente);
 		var_attach_to = createComponente;
        Groups.add(createComponente);
 		layer.add(Groups);
 		layer.add(tr);
 		stage.add(layer);   
 		layer.draw();

 		`)
 }
addComponente('teste1')
addComponente('teste2')




function createInfo(frameWidth, frameHeight, frameX,frameY, _visible=true) {
    var offset = -20;
    var arrowOffset = offset / 2;
    var arrowSize = 5;
    var group = new Konva.Group({visible: _visible});
    

    var leftArrow = new Konva.Shape({
      sceneFunc: function (ctx) {
        /*top pointer*/
        ctx.moveTo(-arrowOffset - arrowSize, arrowSize);
        ctx.lineTo(-arrowOffset, 0);
        ctx.lineTo(-arrowOffset + arrowSize, arrowSize);

        /*line*/
        ctx.moveTo(-arrowOffset, 0);
        ctx.lineTo(-arrowOffset, frameHeight);

        /*bottom pointer*/
        ctx.moveTo(-arrowOffset - arrowSize, frameHeight - arrowSize);
        ctx.lineTo(-arrowOffset, frameHeight);
        ctx.lineTo(-arrowOffset + arrowSize, frameHeight - arrowSize);

        ctx.strokeShape(this);
      },
      stroke: 'grey',
      strokeWidth: 0.5,
      visible: _visible,
      x:frameX,
      y:frameY
    });

    var bottomArrow = new Konva.Shape({
      sceneFunc: function (ctx) {
        /*top pointer*/
        ctx.translate(0, frameHeight + arrowOffset);
        ctx.moveTo(arrowSize, -arrowSize);
        ctx.lineTo(0, 0);
        ctx.lineTo(arrowSize, arrowSize);

        /*line*/
        ctx.moveTo(0, 0);
        ctx.lineTo(frameWidth, 0); /* X*/

        /*bottom pointer*/
        ctx.moveTo(frameWidth - arrowSize, -arrowSize);
        ctx.lineTo(frameWidth, 0);
        ctx.lineTo(frameWidth - arrowSize, arrowSize);

        ctx.strokeShape(this);
      },
      stroke: 'black',
      strokeWidth: 0.5,
      visible: _visible,
      x:frameX,
      y:frameY

    });

    /*left text*/
    var leftLabel = new Konva.Label();

    var leftText = new Konva.Text({
      text: frameHeight + 'mm',
      padding: 2,
      fill: 'black',
      visible: _visible
    });

    leftLabel.add(leftText);
    leftLabel.position({
      x: frameX - (offset/2),
      y: ((frameHeight+frameY )/ 2) - offset
    });




    /*bottom text*/
    var bottomLabel = new Konva.Label();
    var bottomText = new Konva.Text({
      text: frameWidth + 'mm',
      padding: 2,
      fill: 'black'
    });

    bottomLabel.add(bottomText);
    bottomLabel.position({
      x: ((frameX * 2) + (frameWidth/2))/2,
      y: frameHeight + frameY + offset
    });

    group.add(leftArrow, bottomArrow, leftLabel, bottomLabel);

    return group;
  }