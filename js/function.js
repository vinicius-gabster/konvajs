function createFrame(frameWidth, frameHeight) {

			var padding = 0;
			var group = new Konva.Group();
			var glass = new Konva.Rect({
				x: padding,
				y: padding,
				width: frameWidth - padding * 2,
				height: frameHeight - padding * 2,
				fill: 'lightblue',
				visible:true,
				//draggable: true
			});

			group.add(glass);

			group.find('Line')
			.closed(true)
			.stroke('black')
			.strokeWidth(1);

			return group;
		}
		//Altura e largura total do componente
		function createInfo(frameWidth, frameHeight,_visible=true) {
			console.log(frameWidth);
			console.log(frameHeight);
			var offset = -20;

			var arrowOffset = offset / 2;
			var arrowSize = 5;

			var group = new Konva.Group({visible: _visible});
			var lines = new Konva.Shape({
				sceneFunc: function (ctx) {
					ctx.fillStyle = 'grey';
					ctx.lineWidth = 0.5;

					ctx.moveTo(0, 0);
					ctx.lineTo(-offset, 0);

					ctx.moveTo(0, frameHeight);
					ctx.lineTo(-offset, frameHeight);

					ctx.moveTo(0, frameHeight);
					ctx.lineTo(0, frameHeight + offset);

					ctx.moveTo(frameWidth, frameHeight);
					ctx.lineTo(frameWidth, frameHeight + offset);

					ctx.stroke();
				},
				visible: _visible
			});

			var leftArrow = new Konva.Shape({
				sceneFunc: function (ctx) {
					// top pointer
					ctx.moveTo(-arrowOffset - arrowSize, arrowSize);
					ctx.lineTo(-arrowOffset, 0);
					ctx.lineTo(-arrowOffset + arrowSize, arrowSize);

					// line
					ctx.moveTo(-arrowOffset, 0);
					ctx.lineTo(-arrowOffset, frameHeight);

					// bottom pointer
					ctx.moveTo(-arrowOffset - arrowSize, frameHeight - arrowSize);
					ctx.lineTo(-arrowOffset, frameHeight);
					ctx.lineTo(-arrowOffset + arrowSize, frameHeight - arrowSize);

					ctx.strokeShape(this);
				},
				stroke: 'grey',
				strokeWidth: 0.5,
				visible: _visible
			});

			var bottomArrow = new Konva.Shape({
				sceneFunc: function (ctx) {
					// top pointer
					ctx.translate(0, frameHeight + arrowOffset);
					ctx.moveTo(arrowSize, -arrowSize);
					ctx.lineTo(0, 0);
					ctx.lineTo(arrowSize, arrowSize);

					// line
					ctx.moveTo(0, 0);
					ctx.lineTo(frameWidth, 0);

					// bottom pointer
					ctx.moveTo(frameWidth - arrowSize, -arrowSize);
					ctx.lineTo(frameWidth, 0);
					ctx.lineTo(frameWidth - arrowSize, arrowSize);

					ctx.strokeShape(this);
				},
				stroke: 'grey',
				strokeWidth: 0.5,
				visible: _visible
			});

			// left text
			var leftLabel = new Konva.Label();

			var leftText = new Konva.Text({
				text: heightInput.value + 'mm',
				padding: 2,
				fill: 'black',
				visible: _visible
			});

			leftLabel.add(leftText);
			leftLabel.position({
				x: leftText.x() + 10,
				y: frameHeight / 2 - leftText.height() / 2
			});


			

			// bottom text
			var bottomLabel = new Konva.Label();
			var bottomText = new Konva.Text({
				text: widthInput.value + 'mm',
				padding: 2,
				fill: 'black'
			});

			bottomLabel.add(bottomText);
			bottomLabel.position({
				x: frameWidth / 2 - bottomText.width() / 2,
				y: frameHeight - 25
			});

			group.add(lines, leftArrow, bottomArrow, leftLabel, bottomLabel);

			return group;
		}
		
		function updateCanvas(visible) {

			layer.children.destroy();           

			var frameWidth = parseInt(widthInput.value, 10);
			var frameHeight = parseInt(heightInput.value, 10);

			var wr = stage.width() / frameWidth;
			var hr = stage.height() / frameHeight;

			//var ratio = Math.min(wr, hr) * 0.8;
			var ratio = 1;

			var frameOnScreenWidth = frameWidth * ratio;
			var frameOnScreenHeight = frameHeight * ratio;
			console.log('frameOnScreenWidth: '+frameOnScreenWidth)
			console.log('frameOnScreenHeight: '+frameOnScreenHeight)

			/*group.x(Math.round(stage.width() / 2 - frameOnScreenWidth / 2) + 0.5);
			group.y(Math.round(stage.height() / 2 - frameOnScreenHeight / 2) + 0.5);*/

			group.y(50)
			group.x(50)


			var frameGroup = createFrame(frameWidth, frameHeight);
			frameGroup.scale({ x: ratio, y: ratio });
			group.add(frameGroup);

			var infoGroup = createInfo(frameOnScreenWidth, frameOnScreenHeight,visible);
			group.add(infoGroup);


			test = group;

			layer.on('dragmove', function(e) {
				tr.detach(var_attach_to);
				var target = e.target;
				var targetRect = e.target.getClientRect();

				let arr = layer.children;
				let arrayRects = [];
				let _lastPosValid = undefined;

				arr.each((x)=>{

					if (x.getAttr('isGBS')) {
						arrayRects.push(x);  
					}
				});

				for (var i = arrayRects.length - 1; i >= 0; i--) {
					let arrX1 = arrayRects[i].getClientRect().x  
					let arrY1 = arrayRects[i].getClientRect().y  
					let arrWidth = arrayRects[i].getClientRect().width
					let arrHeight = arrayRects[i].getClientRect().height

					let objX1 = targetRect.x  
					let objY1 = targetRect.y  
					let objWidth = targetRect.width
					let objHeight = targetRect.height

					if (arrX1 == objX1 && arrY1 == objY1 && arrWidth == objWidth && arrHeight == objHeight) {

					}else{
						let intersection = haveIntersection(arrayRects[i].getClientRect(),targetRect);
						if (intersection) {
							target.setAbsolutePosition({x:pos_valid.x,y:pos_valid.y});
						} else {
							pos_valid = target._lastPos;
						}
					}
				}
			});


			group.on('click', function() {
				tr.detach(var_attach_to);
				var_attach_to = this;
				tr.attachTo(this);
			});

			tr.attachTo(group);
			layer.add(group);
			layer.draw();



		}


		function updateFrame(componente,visible=true) {

			componente.destroy();           
			console.log(componente)
			var frameWidth = parseInt(componente.attrs.width, 10);
			var frameHeight = parseInt(componente.attrs.height, 10);

			var wr = stage.width() / frameWidth;
			var hr = stage.height() / frameHeight;

			//var ratio = Math.min(wr, hr) * 0.8;
			var ratio = 1;

			var frameOnScreenWidth = frameWidth * ratio;
			var frameOnScreenHeight = frameHeight * ratio;
			console.log('frameOnScreenWidth: '+frameOnScreenWidth)
			console.log('frameOnScreenHeight: '+frameOnScreenHeight)

			/*group.x(Math.round(stage.width() / 2 - frameOnScreenWidth / 2) + 0.5);
			group.y(Math.round(stage.height() / 2 - frameOnScreenHeight / 2) + 0.5);*/

			group.y(50)
			group.x(50)


			var frameGroup = createFrame(frameWidth, frameHeight);
			frameGroup.scale({ x: ratio, y: ratio });
			group.add(frameGroup);

			var infoGroup = createInfo(frameOnScreenWidth, frameOnScreenHeight,visible);
			group.add(infoGroup);


			test = group;

			layer.on('dragmove', function(e) {
				tr.detach(var_attach_to);
				var target = e.target;
				var targetRect = e.target.getClientRect();

				let arr = layer.children;
				let arrayRects = [];
				let _lastPosValid = undefined;

				arr.each((x)=>{

					if (x.getAttr('isGBS')) {
						arrayRects.push(x);  
					}
				});

				for (var i = arrayRects.length - 1; i >= 0; i--) {
					let arrX1 = arrayRects[i].getClientRect().x  
					let arrY1 = arrayRects[i].getClientRect().y  
					let arrWidth = arrayRects[i].getClientRect().width
					let arrHeight = arrayRects[i].getClientRect().height

					let objX1 = targetRect.x  
					let objY1 = targetRect.y  
					let objWidth = targetRect.width
					let objHeight = targetRect.height

					if (arrX1 == objX1 && arrY1 == objY1 && arrWidth == objWidth && arrHeight == objHeight) {

					}else{
						let intersection = haveIntersection(arrayRects[i].getClientRect(),targetRect);
						if (intersection) {
							target.setAbsolutePosition({x:pos_valid.x,y:pos_valid.y});
						} else {
							pos_valid = target._lastPos;
						}
					}
				}
			});


			group.on('click', function() {
				tr.detach(var_attach_to);
				var_attach_to = this;
				tr.attachTo(this);
			});

			tr.attachTo(group);
			layer.add(group);
			layer.draw();

		}


		