let LAYER = [];
let div2 = 0;
let teste = undefined;
const contador = new Contador();
createLayer('principal',1000,700)

async function saveProject(idLayer) {
    let data = LAYER[idLayer]['layer'].children[1].attrs;

    data = JSON.stringify(data);
    data = JSON.parse(data);

    let json = { 
        "id" :5,
        "componente" : {
            "x": 10,
            "y": 10,
            "attrs": data
        }
    }
    console.log(json)

    json = JSON.stringify(json)
    //json = JSON.parse(json);

    console.log(json)

    let req = await $.ajax({
        type: "POST",
        url: "http://localhost:3001/konvajs",
        contentType: 'applicationjson',
        data: JSON.stringify(json),
        success: async function (data){
            await console.log(data);
        },
        error: function(data){
            alert("error")
        }
    })

    return await req;
}



function addLayer(){
    let random = Math.round(Math.random() * 19711);
    let parede = 'parede'+random;
    createLayer(parede.toString(),1500,900);
}

function createLayer(nome_parede,lenX,lenY){
    let view = $('.container-fluid');
    let lenX_view = view.width();
    let lenY_view = window.innerHeight;

    let razaoY = lenY_view / lenY;
    let razaoX = lenX_view / lenX;

    let razao = undefined;
    let RazaoInterna = 1;
    console.log(lenX_view,lenY_view);
    console.log(razaoX,razaoY);

    if (razaoX < razaoY) {
        razao = razaoX;
        RazaoInterna = razaoY;
    } else {
        razao = razaoY;
        RazaoInterna = razaoX;

    }
    lenX = lenX * razao;
    lenY = lenY * razao;
    let marginX = Math.abs((lenX - lenX_view)) / 2;
    let marginY = Math.abs((lenY - lenY_view)) / 2;


    let width = lenX;
    let height = lenY;

    const contadorLayer = contador.Increment();
    $('.layers').append(`
        <div class="content-layer ${nome_parede}">
        <div class="buttons">
        <button class="waves-effect waves-light btn btn-adicionar" onclick="AddComponente(${contadorLayer},${nome_parede})"><i class="fas fa-plus-circle"></i></button>
        </div>
        <div id=${nome_parede} class="paredes_container" style="width: ${Math.round(width)}px!important;height: ${Math.round(height)}px!important">

        </div>
        </div>

        <style>

        #${nome_parede} .paredes_container{
        }

        </style>
        `);


    LAYER[`${contadorLayer}`] = [];
    LAYER[`${contadorLayer}`]['key'] = undefined;
    LAYER[`${contadorLayer}`]['ESCALA'] = razao;
    LAYER[`${contadorLayer}`]['DELTA'] = 1;
    LAYER[`${contadorLayer}`]['posValid'] = 0;
    LAYER[`${contadorLayer}`]['attachTranformer'] = undefined;
    LAYER[`${contadorLayer}`]['verifyIntersection']  = false;
    LAYER[`${contadorLayer}`]['stage'] = new Konva.Stage({
        container: nome_parede,
        width: width,
        height: height
    });

    LAYER[`${contadorLayer}`]['layer'] = new Konva.Layer();
    LAYER[`${contadorLayer}`]['stage'].add(LAYER[`${contadorLayer}`]['layer']);
    LAYER[`${contadorLayer}`]['tr'] = new Konva.Transformer({
        relativeToDiv:nome_parede,
        isTransformer:true,
        rotateEnabled:false,
        boundBoxFunc: function (oldBoundBox, newBoundBox) {
            let node = LAYER[`${contadorLayer}`]['tr'].getNode();
            let imagemProduto = node.attrs.img;
            console.log('imagemProduto'+imagemProduto)
            MAX_WIDTH  = document.getElementById(LAYER[`${contadorLayer}`]['tr'].attrs.relativeToDiv).clientWidth;
            MAX_HEIGHT = document.getElementById(LAYER[`${contadorLayer}`]['tr'].attrs.relativeToDiv).clientHeight;

            if (newBoundBox.x < 0) {
                newBoundBox.x = 0
                newBoundBox.width = oldBoundBox.width;
            }

            if (newBoundBox.y < 0 ) {
                newBoundBox.y = 0
                newBoundBox.height = oldBoundBox.height;
            }

            if (newBoundBox.x + newBoundBox.width > MAX_WIDTH || newBoundBox.y + newBoundBox.height > MAX_HEIGHT) {
                newBoundBox = oldBoundBox;
            }

            LAYER[`${contadorLayer}`]['layer'].children.each(function(child){
                if (child === node) {
                    return;
                }

                if (child.getAttr('isTransformer')) {
                    return;
                }
                if (haveIntersection(node.getClientRect(), child.getClientRect())) {
                    newBoundBox = oldBoundBox;
                }  
            });

            let componente = LAYER[`${contadorLayer}`]['tr'].getNode();

            teste = componente;

            //if (newBoundBox.x > 0 && newBoundBox.y > 0 && newBoundBox.x + newBoundBox.width < MAX_WIDTH && newBoundBox.y + newBoundBox.height < MAX_HEIGHT) {
                let newComponent = createShape(newBoundBox.width,newBoundBox.height,LAYER[`${contadorLayer}`]['tr'].attrs.relativeToDiv,contadorLayer, imagemProduto);

                LAYER[`${contadorLayer}`]['layer'].children[componente.index].destroy();
                LAYER[`${contadorLayer}`]['layer'].add(newComponent);

                LAYER[`${contadorLayer}`]['tr'].setNode(newComponent);  
            //}

            LAYER[`${contadorLayer}`]['layer'].draw();

            return newBoundBox
        }
    });

    LAYER[`${contadorLayer}`]['layer'].add(LAYER[`${contadorLayer}`]['tr']);
    LAYER[`${contadorLayer}`]['layer'].draw();

    LAYER[`${contadorLayer}`]['layer'].on('click', function(e){
        e.target.parent.posX = e.target.parent.getX(); 
        e.target.parent.posY = e.target.parent.getY();
        teste = e.target.parent
        $('.buttons .btn-remove').remove();
        $(`.${nome_parede} .buttons`).append(`<button class="btn-remove waves-effect waves-light btn red accent-1" onclick="deleteElement(${e.target.parent.index},${contadorLayer})"><i class="fas fa-window-close"></i></button>`);
        LAYER[`${contadorLayer}`]['tr'].detach(LAYER[`${contadorLayer}`]['attachTranformer']);
        LAYER[`${contadorLayer}`]['attachTranformer'] = e.target.parent;
        LAYER[`${contadorLayer}`]['key'] = LAYER[`${contadorLayer}`]['attachTranformer'];
        LAYER[`${contadorLayer}`]['tr'].attachTo(LAYER[`${contadorLayer}`]['attachTranformer']);
        LAYER[`${contadorLayer}`]['layer'].draw();
    });

    LAYER[`${contadorLayer}`]['stage'].container().tabIndex = -1;
    LAYER[`${contadorLayer}`]['stage'].container().addEventListener('keydown', function (e) {

        let width1 = LAYER[`${contadorLayer}`]['key'].findOne('.fillShape').width();
        //console.log('width: '+width1)

        if (e.keyCode === 37) {
            //console.log('37')
            if (LAYER[`${contadorLayer}`]['layer'].getAllIntersections({x:LAYER[`${contadorLayer}`]['key'].x() - LAYER[`${contadorLayer}`]['DELTA'],y:LAYER[`${contadorLayer}`]['key'].y()}).length < 3) {
                LAYER[`${contadorLayer}`]['key'].x(LAYER[`${contadorLayer}`]['key'].x() - LAYER[`${contadorLayer}`]['DELTA']);
            }

        } else if (e.keyCode === 38) {
            //console.log('38')

            if (LAYER[`${contadorLayer}`]['layer'].getAllIntersections({y:LAYER[`${contadorLayer}`]['key'].y() - LAYER[`${contadorLayer}`]['DELTA'],x:LAYER[`${contadorLayer}`]['key'].x()}).length < 3) {
                LAYER[`${contadorLayer}`]['key'].y(LAYER[`${contadorLayer}`]['key'].y() - LAYER[`${contadorLayer}`]['DELTA']);
            }


        } else if (e.keyCode === 39) {
            //console.log('39')

            if (LAYER[`${contadorLayer}`]['layer'].getAllIntersections({x:LAYER[`${contadorLayer}`]['key'].x() + LAYER[`${contadorLayer}`]['key'].width() + LAYER[`${contadorLayer}`]['DELTA'],y:LAYER[`${contadorLayer}`]['key'].y()}).length <= 3) {
                LAYER[`${contadorLayer}`]['key'].x(LAYER[`${contadorLayer}`]['key'].x() + LAYER[`${contadorLayer}`]['DELTA']);
            }
        } else if (e.keyCode === 40) {
            //console.log('40')

            if (LAYER[`${contadorLayer}`]['layer'].getAllIntersections({y: LAYER[`${contadorLayer}`]['key'].y() + LAYER[`${contadorLayer}`]['key'].height() + LAYER[`${contadorLayer}`]['DELTA'], x:LAYER[`${contadorLayer}`]['key'].x()}).length <= 3) {
                LAYER[`${contadorLayer}`]['key'].y(LAYER[`${contadorLayer}`]['key'].y() + LAYER[`${contadorLayer}`]['DELTA']);
            }
        } else {
            return;
        }

        e.preventDefault();
        LAYER[`${contadorLayer}`]['layer'].batchDraw();
    });

    LAYER[`${contadorLayer}`]['layer'].on('dragstart', function(e) {
        LAYER[`${contadorLayer}`]['tr'].detach(LAYER[`${contadorLayer}`]['attachTranformer']);
        LAYER[`${contadorLayer}`]['verifyIntersection'] = false;
        var target = e.target;
        var targetRect = e.target.getClientRect();
        var Intersection = false;

        LAYER[`${contadorLayer}`]['posValid'] = {x:e.target.attrs.x, y:e.target.attrs.y};
    });

    LAYER[`${contadorLayer}`]['layer'].on('dragend', function(e) {
        var target = e.target;
        var targetRect = e.target.getClientRect();
        var children = LAYER[`${contadorLayer}`]['layer'].children;
        var Intersection = false;
        LAYER[`${contadorLayer}`]['layer'].children.each(function(group){

            if (group === target) {
                return;
            }

            if (group.attrs.isTransformer) {
                return;
            }

            if (haveIntersection(group.getClientRect(), targetRect)) {
                target.setX(LAYER[`${contadorLayer}`]['posValid'].x);
                target.setY(LAYER[`${contadorLayer}`]['posValid'].y);
                LAYER[`${contadorLayer}`]['layer'].draw();
                Intersection = true;
                LAYER[`${contadorLayer}`]['verifyIntersection'] = true;

            } else {
                if (!LAYER[`${contadorLayer}`]['verifyIntersection']) {
                    target.posX = target.getX(); 
                    target.posY = target.getY();
                    LAYER[`${contadorLayer}`]['verifyIntersection'] = false; 
                }
            }
        });
        LAYER[`${contadorLayer}`]['posValid'] = undefined;
    });
}

function Contador(nome_parede){
    let contador = 0;

    this.Increment = function(){
        contador++;
        return contador;
    }
}

function AddComponente(idLayer,div,img=null) {
    console.clear();
    if (!img) {
        img = "https://www.minhamobilia.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/m/o/mobilia_moderna_cadeira_imperial_ref_1010-0033_2.jpg";
    }
    $(`.btn-adicionar`).prop('disabled', true);

    let mousemovePosition = undefined;
    let newComponente = createShape(100,100,div.id,idLayer,img);

    LAYER[idLayer]['layer'].add(newComponente);
    LAYER[idLayer]['layer'].draw();

    LAYER[idLayer]['stage'].on('mousemove', function() {
        LAYER[idLayer]['tr']._node = null;
        LAYER[idLayer]['layer'].draw();
        console.log(LAYER[idLayer]['tr'])
        $(`.btn-adicionar`).prop('disabled', true);

        let mousePos = LAYER[idLayer]['stage'].getPointerPosition();
        let x = mousePos.x;
        let y = mousePos.y
        let ImageComponent = newComponente.findOne('.fillShape');
        console.log(ImageComponent)

        try{
            MAX_WIDTH  = document.getElementById(`${div.id}`).clientWidth - ImageComponent.width();
            MAX_HEIGHT = document.getElementById(`${div.id}`).clientHeight - ImageComponent.height();
        }catch(erro){
            console.log(erro);
            MAX_WIDTH =  $('.konvajs-content')[0].clientWidth - ImageComponent.width();
            MAX_HEIGHT = $('.konvajs-content')[0].clientHeight - ImageComponent.height()
        }


        let pos = {x:x, y:y};

        let x_abs = pos.x
        let y_abs = pos.y;

        if (pos.x < 0 ) {
            x_abs = 0;
        }

        if (pos.y < 0 ) {
            y_abs = 0;
        }

        if ( pos.y + newComponente.height() >= MAX_HEIGHT ) {
            y_abs = MAX_HEIGHT - newComponente.height();
        }

        if ((newComponente.width()) + pos.x >= MAX_WIDTH ) {
            x_abs = MAX_WIDTH - newComponente.width();

        }

        x = x_abs;
        y = y_abs;

        mousemovePosition = {x: x  ,  y: y   ,  width:  100 ,  height:  100 }; 


        newComponente.setX(x);        
        newComponente.setY(y);

        LAYER[idLayer]['layer'].draw();
    });


    LAYER[idLayer]['stage'].on('click', function(e) {
        //e.evt.preventDefault();

        let verifyIntersection = false;
        let target = e.target.parent;
        if (!target) {
            target = newComponente;
        }
        try{
            if (LAYER[idLayer]['layer'].children.length > 2 ) {

                LAYER[idLayer]['layer'].children.each(function(group){

                    if (group === target) {
                        return;
                    }

                    if (group.attrs.isTransformer) {
                        return;
                    }

                    if(haveIntersection(group.getClientRect(), target.getClientRect())){

                        console.log('Intersection');
                        verifyIntersection = true;
                    }     
                });
            }else{
                LAYER[idLayer]['stage'].removeEventListener('mousemove');
                console.log('break mousemove');
                $(`.btn-adicionar`).removeAttr('disabled');
                
            }

            if (!verifyIntersection) {
                console.log('break mousemove 22');

                LAYER[idLayer]['stage'].removeEventListener('mousemove');
                $(`.btn-adicionar`).removeAttr('disabled');

            }
        }catch(erro){
            console.log(erro);
        }
    });
}

function deleteElement(index,contador){
    LAYER[contador]['tr'].detach(LAYER[contador]['attachTranformer']);
    LAYER[contador]['layer'].children[index].destroy();
    LAYER[contador]['layer'].draw();
    $('.buttons .btn-remove').remove();
}

function haveIntersection(r1, r2) {
    return !(
        r2.x >= r1.x + r1.width  - 1  ||
        r2.x + r2.width <= r1.x  + 1  ||
        r2.y >= r1.y + r1.height - 1 ||
        r2.y + r2.height <= r1.y + 2
        );
}

function createShape(width=1,height=1,idDiv,idLayer,img) {
    console.log('idDiv')
    console.log(idDiv)
    var group = new Konva.Group({
        relativeToDiv:idDiv,
        name:'groupPrincipal',
        x: Math.random() * width,
        y: Math.random() * height,
        img:img,
        draggable: true,
        dragBoundFunc: function(pos) {
            try{
                MAX_WIDTH = document.getElementById(`${idDiv}`).clientWidth;
                MAX_HEIGHT = document.getElementById(`${idDiv}`).clientHeight;
            }catch(erro){

                console.log(this);

                MAX_WIDTH = document.getElementById(`${this.attrs.relativeToDiv}`).clientWidth;
                MAX_HEIGHT = document.getElementById(`${this.attrs.relativeToDiv}`).clientHeight;
            }

            let x_abs = pos.x
            let y_abs = pos.y;

            if (pos.x < 0 ) {
                x_abs = 0;
            }

            if (pos.y < 0 ) {
                y_abs = 0;
            }

            let findOne = this.findOne('.fillShape');

            if ( pos.y + (this.findOne('.fillShape').getClientRect().height) >= MAX_HEIGHT ) {
                y_abs = MAX_HEIGHT - this.findOne('.fillShape').getClientRect().height;
            }

            if ((this.findOne('.fillShape').getClientRect().width)+ pos.x >= MAX_WIDTH ) {
                x_abs = MAX_WIDTH - (this.findOne('.fillShape').getClientRect().width);

            }

            return {
                x: x_abs,
                y: y_abs
            }
        }
    });

    let ScaleLayer =  LAYER[idLayer]['ESCALA'].toFixed(1);
    ScaleLayer = ((1 - ScaleLayer) + 1);


    console.log('ScaleLayer ' + ScaleLayer);

    var shape = new Konva.Image({
        height: height,
        width: width,
        scale: ScaleLayer,
        fill: '#00000040',
        name: 'fillShape',
    });

    var imageObj1 = new Image();
    imageObj1.onload = function() {
        shape.image(imageObj1);
        //layer.draw();
    };
    imageObj1.src = img;



    var label = createInfo(shape.attrs.width,shape.attrs.height,ScaleLayer);
    group.add(shape);
    group.add(label);

    var boundingBox = shape.getClientRect({ relativeTo: group });

    var box = new Konva.Rect({
        x: boundingBox.x,
        y: boundingBox.y,
        width: boundingBox.width,
        height: boundingBox.height,
    });
    //group.add(box);
    group.posX = group.x(); 
    group.posY = group.y();

    console.log('group')
    console.log(group)
    return group;
}

function createInfo(frameWidth, frameHeight,razao,_visible=true) {
    /*//console.log(frameWidth);    //console.log(frameHeight);*/
    var offset = -20;

    var arrowOffset = offset / 2;
    var arrowSize = 10;
    var stroke = 0.0;
    var lineWidth = 0.0;

    var group = new Konva.Group({visible: _visible,name:'groupLabels'});
    var leftLabel = new Konva.Label({rotation: 90});

    var leftText = new Konva.Text({
        fontSize:12,
        text: Math.round((frameHeight )* razao)  + 'mm',
        padding: 2,
        fill: 'black',
        visible: _visible,
        name: 'leftText'
    });

    leftLabel.add(leftText);
    leftLabel.position({
        x: leftText.x() + 20,
        y: frameHeight / 2 - leftText.height() / 2
    });


    var bottomLabel = new Konva.Label({});
    
    var bottomText = new Konva.Text({
        fontSize:12,
        text: Math.round((frameWidth) * razao )  + 'mm',
        padding: 2,
        fill: 'black',
        naPme: 'bottomText'

    });

    bottomLabel.add(bottomText);
    bottomLabel.position({
        x: frameWidth / 2 - bottomText.width() / 2,
        y: frameHeight - 25
    });

    group.add(leftLabel, bottomLabel);

    return group;
}

async function getLayerDB(idLayer,idDiv){

    let response = await AJAXCompnentsInProject();

    $.each(response,function(key, KONVA){
    console.log(KONVA);

    KONVA = KONVA.componente;


            var group = new Konva.Group({
                relativeToDiv: KONVA.attrs.relativeToDiv,
                name:KONVA.attrs.name,
                x: KONVA.x,
                y: KONVA.y,
                img:KONVA.attrs.img,
                draggable: KONVA.attrs.draggable,
                dragBoundFunc: function(pos) {
                    try{
                        MAX_WIDTH = document.getElementById(`${idDiv}`).clientWidth;
                        MAX_HEIGHT = document.getElementById(`${idDiv}`).clientHeight;
                    }catch(erro){

                        console.log(this);

                        MAX_WIDTH = document.getElementsByClassName(`${this.attrs.relativeToDiv}`).clientWidth;
                        MAX_HEIGHT = document.getElementsByClassName(`${this.attrs.relativeToDiv}`).clientHeight;
                    }

                    let x_abs = pos.x
                    let y_abs = pos.y;

                    if (pos.x < 0 ) {
                        x_abs = 0;
                    }

                    if (pos.y < 0 ) {
                        y_abs = 0;
                    }

                    let findOne = this.findOne('.fillShape');

                    if ( pos.y + (this.findOne('.fillShape').getClientRect().height) >= MAX_HEIGHT ) {
                        y_abs = MAX_HEIGHT - this.findOne('.fillShape').getClientRect().height;
                    }

                    if ((this.findOne('.fillShape').getClientRect().width)+ pos.x >= MAX_WIDTH ) {
                        x_abs = MAX_WIDTH - (this.findOne('.fillShape').getClientRect().width);

                    }

                    return {
                        x: x_abs,
                        y: y_abs
                    }
                }

            })


            let ScaleLayer =  LAYER[idLayer]['ESCALA'].toFixed(1);
            ScaleLayer = ((1 - ScaleLayer) + 1);


            console.log('ScaleLayer ' + ScaleLayer);

            var shape = new Konva.Image({
                height: KONVA.children[0].attrs.height,
                width: KONVA.children[0].attrs.width,
                scale: ScaleLayer,
                fill: '#00000040',
                name: 'fillShape',
            });

            var imageObj1 = new Image();
            imageObj1.onload = function() {
                shape.image(imageObj1);
                    //layer.draw();
                };
                imageObj1.src = KONVA.attrs.img;



                var label = createInfo(shape.attrs.width,shape.attrs.height,ScaleLayer);
                group.add(shape);
                group.add(label);

                var boundingBox = shape.getClientRect({ relativeTo: group });

                var box = new Konva.Rect({
                    x: boundingBox.x,
                    y: boundingBox.y,
                    width: boundingBox.width,
                    height: boundingBox.height,
                });
                //group.add(box);
                group.posX = group.x(); 
                group.posY = group.y();

                console.log('group')
                console.log(group)
                LAYER[idLayer].layer.add(group);
                LAYER[idLayer].layer.draw();
                return group;
    });
}

async function AJAXComponentsInProject() {

    return await $.ajax({
        type: "GET",
        url: "http://localhost:3001/konvajs",
        contentType: 'application/json',
        success: function (data){ 
            console.log(data)
        }
    })

}

async function AJAXBibliotecas() {
    let req = await $.ajax({
        type: "GET",
        url: URL_API + "/ws/b/c/?format=json",
        contentType: 'json',
        headers: {
            "Authorization": "ApiKey "+USER+":"+APIKEY+"",
            "Content-Type" : 'application/json',
        },
        success: async function (data){
            await console.log(data.status.bibliotecas[0]);
        },
        error: function(data){
            alert("error")
        }
    })

    return await req;
}

async function AJAXOpcaoComponente(idProduto){
    let request = await $.ajax({
        type: "GET",
        url: URL_API + "/api/v1/via_opcao_componente/?componente="+idProduto+"&format=json",
        contentType: 'json',
        headers: {
            "Authorization": "ApiKey "+USER+":"+APIKEY+"",
            "Content-Type" : 'application/json',
        },
        success: async function (data){
            await console.log(data)
            
        }
    });

    return request;
}


/* AJAX */
const URL_API = 'http://127.0.0.1:8000';
const APIKEY  = 'e76d1238facc70019bea81037a92eca701002611';
const USER    =  'vinicius.silva@gabster.com.br';