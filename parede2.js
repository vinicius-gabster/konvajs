var attachTranformer = undefined;
var verifyIntersection = false;
var width = window.innerWidth;
var height = window.innerHeight;

var stage2 = new Konva.Stage({
	container: 'parede02',
	width: width,
	height: height
});

var layer2 = new Konva.Layer();
stage2.add(layer2);

var tr2 = new Konva.Transformer({
	isTransformer:true,
	boundBoxFunc: function (oldBoundBox, newBoundBox) {
		let node = tr2.getNode();

		layer2.children.each(function(child){
			if (child === node) {
				return;
			}

			if (child.getAttr('isTransformer')) {
				console.log('Transformer');
				return;
			}
			console.log('_');
			console.log(child);
			console.log(node);

			if (haveIntersection(node.getClientRect(), child.getClientRect())) {
				newBoundBox = oldBoundBox;
			}

		});

        //console.log(tr.getNode().find('.bottomText'))
        tr2.getNode().find('.bottomText').setText(Math.round(newBoundBox.width)+'mm')
        tr2.getNode().find('.leftText').setText(Math.round(newBoundBox.height)+'mm')
        layer2.draw();

        return newBoundBox
    }
});

layer2.add(tr2);
layer2.draw();



    layer2.on('click', function(e){
      tr2.detach(attachTranformer);
      attachTranformer = e.target.parent;
      tr2.attachTo(attachTranformer);
      layer2.draw();
    });

    layer2.on('dragstart', function(e) {
      tr2.detach(attachTranformer);

      verifyIntersection = false;

    });

    layer2.on('dragmove', function(e) {
      var target = e.target;
      var targetRect = e.target.getClientRect();

      var children = layer2.children;
      var Intersection = false;

      layer2.children.each(function(group){

        if (group === target) {
          return;
        }
        if (group.attrs.isTransformer != undefined) {
          return;
        }

        if (haveIntersection(group.getClientRect(), targetRect)) {
          /*group.findOne('.fillShape').fill('red');
          target.findOne('.fillShape').fill('black');*/
          //target.setY(target.posY);
          //target.setX(target.posX);
          console.log('Collision')
          layer2.draw();

          Intersection = true;
          verifyIntersection = true;

        } else {

          console.log(verifyIntersection);

          if (!verifyIntersection) {
            console.log('Collision')

           /* group.findOne('.fillShape').fill('grey');
            target.findOne('.fillShape').fill('grey');*/
            target.posX = target.getX(); 
            target.posY = target.getY();
            verifyIntersection = false; 
          }

        }
      });
    });

    layer2.on('dragend', function(e) {
      var target = e.target;
      var targetRect = e.target.getClientRect();

      var children = layer2.children;
      var Intersection = false;

      layer2.children.each(function(group){

        if (group === target) {
          return;
        }
        if (haveIntersection(group.getClientRect(), targetRect)) {
          /*group.findOne('.fillShape').fill('red');
          target.findOne('.fillShape').fill('black');*/
          target.setX(target.posX);
          target.setY(target.posY);
          console.log('Collision')
          layer2.draw();

          Intersection = true;
          verifyIntersection = true;

        } else {

          console.log(verifyIntersection);

          if (!verifyIntersection) {
            console.log('Collision')

            group.findOne('.fillShape').fill('grey');
            target.findOne('.fillShape').fill('grey');
            target.posX = target.getX(); 
            target.posY = target.getY();
            verifyIntersection = false; 
          }

        }
      });
    });